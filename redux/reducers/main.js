import { SET_NAME, SET_CURRENT_USER } from '../types';

const initialCurrentUser = () => {
  let data = {};

  if (typeof window !== 'undefined') {
    if (localStorage.getItem('currentUser')) {
      data = JSON.parse(localStorage.getItem('currentUser'));
    } else {
      data = null;
    }
  } else {
    data = null;
  }

  return data;
};

const initialState = {
  user: {
    id: 1,
    name: 'Ilham',
    phoneNumber: '081949565427',
  },
  currentUser: initialCurrentUser(),
};

const main = (state = initialState, action) => {
  switch (action.type) {
    case SET_NAME:
      return { ...state, user: action.payload };
    case SET_CURRENT_USER:
      return { ...state, currentUser: action.payload };
    default:
      return { ...state };
  }
};

export default main;
