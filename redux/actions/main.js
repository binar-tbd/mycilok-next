import * as t from '../types';

export const setInfo = (id, name, phoneNumber) => (dispatch) => {
  dispatch({
    type: t.SET_NAME,
    payload: id && name && phoneNumber,
  });
};

export const setCurrentUser = (payload) => (dispatch) => {
  localStorage.setItem('currentUser', JSON.stringify(payload));

  dispatch({
    type: t.SET_CURRENT_USER,
    payload,
  });
};
