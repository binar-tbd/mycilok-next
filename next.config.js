module.exports = {
  reactStrictMode: true,
  images: {
    domains: [
      'static.vecteezy.com',
      'wallpapercave.com',
      'media.contentapi.ea.com',
      'localhost',
      'res.cloudinary.com',
      '',
    ],
  },
};
