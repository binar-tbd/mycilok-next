// eslint-disable-next-line no-unused-vars
import { render } from '@testing-library/react';
import Header from '../components/Header/index.jsx';

describe('App', () => {
  it('should navigate to the home page', () => {
    const { getByText } = render(<Header />);
    expect(getByText('Home').href).toBe('http://localhost/');
  });
  it('should navigate to the blog page', () => {
    const { getByText } = render(<Header />);
    expect(getByText('Blog').href).toBe('http://localhost/blog');
  });
  it('should navigate to the detail page', () => {
    const { getByText } = render(<Header />);
    expect(getByText('Detail').href).toBe('http://localhost/users');
  });
  it('should navigate to the game page', () => {
    const { getByText } = render(<Header />);
    expect(getByText('Game').href).toBe('http://localhost/game');
  });
});
