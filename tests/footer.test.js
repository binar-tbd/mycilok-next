// eslint-disable-next-line no-unused-vars
import { render } from "@testing-library/react";
import Footer from "../components/Footer/Footer.jsx";

describe("App", () => {
  it("should render the text", () => {
    const { getByText } = render(<Footer />);
    expect(getByText(/made with warranty @MyCilok/i)).toBeInTheDocument();
  });
});
