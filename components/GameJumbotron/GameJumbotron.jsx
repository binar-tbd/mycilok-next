/* eslint-disable react/state-in-constructor */
import React, { Component } from 'react';
import './GameJumbotron.module.css';

class GameJumbotron extends Component {
  state = {};

  render() {
    return (
      <div className='myLanding mt-1 shadow-lg'>
        <div className='container-fluid shadow-lg bg-info mx-auto px-5 py-5 text-dark rounded-3'>
          <p className='display-4 px-1 py-2'>MyCilok Games Portal</p>
          <p className='lead'>
            Welcome to MyCilok Games Portal! Play PC Games of Every genre. Games for everyone.
          </p>
          <hr className='my-2 text-white' />
          <p className='text-white'>
            Lets Play!
          </p>
          <p className='lead'>
            <a className='btn btn-warning shadow-lg' href='#two'>
              Choose Game
            </a>
          </p>
        </div>
      </div>
    );
  }
}

export default GameJumbotron;
