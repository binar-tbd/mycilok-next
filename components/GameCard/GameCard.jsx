/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable react/state-in-constructor */
import React, { Component } from 'react';
import Link from 'next/link';
import Image from 'next/image';
import { connect } from 'react-redux';

class GameCard extends Component {
  state = {};

  render() {
    const {
      img, title, detail, uuid, currentUser,
    } = this.props;
    const gameTitle = title === 'RPS' ? 'Play' : 'Cooming soon';
    const gameClass = gameTitle === 'Cooming soon' || !currentUser
      ? 'btn btn-warning shadow-lg disabled'
      : 'btn btn-warning shadow-lg';
    const gameImage = '/image/game2.jpg';

    return (
      <div
        className='card mx-auto shadow-lg mt-3 bg-dark text-white px-0'
        style={{
          width: '23rem',
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
        }}
      >
        <Image
          src={img || gameImage}
          className='card-img-top'
          alt='...'
          width={380}
          height={250}
        />
        <div className='card-body'>
          <h5 className='card-title'>{title}</h5>
          <p className='card-text'>{detail}</p>
          {/* <p className="btn-danger">{uuid}</p> */}
          <div className='pl-0'>
            <Link href={`/game/${uuid}`}>
              <a className='btn btn-info shadow-lg me-2'>Game Detail</a>
            </Link>
            <Link href={`/game/play/${uuid}`}>
              <a className={gameClass}>{gameTitle}</a>
            </Link>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({ currentUser: state.main?.currentUser });

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(GameCard);
