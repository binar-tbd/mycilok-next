import styles from './Footer.module.css';

export default function Footer() {
  return (
    <div className={styles.background}>
      <p className={styles.title}>made with warranty @MyCilok</p>
    </div>
  );
}
