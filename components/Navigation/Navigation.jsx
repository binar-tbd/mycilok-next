/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable no-shadow */
import React, { useEffect, useState } from 'react';
import { Container, Nav, Navbar } from 'react-bootstrap';
import Link from 'next/link';
import Image from 'next/image';
import { connect } from 'react-redux';
import { useRouter } from 'next/router';
import firebase from '../services/firebase';
import { setCurrentUser } from '../../redux/actions/main';
import styles from './Navigation.module.css';

const Navigation = (props) => {
  const { currentUser, setCurrentUser } = props;
  const [isLogin, setIsLogin] = useState(false);
  const router = useRouter();

  const isLoginHandler = () => {
    if (currentUser) {
      setIsLogin(true);
    } else {
      setIsLogin(false);
    }
  };

  const handleLogout = async () => {
    const answer = window.confirm('Anda yakin ingin logout?');
    if (answer) {
      await firebase.auth().signOut();
      setCurrentUser(null);
      isLoginHandler();
      router.push('/');
      alert('Anda telah logout!');
    } else {
      alert('Please enjoy our game!');
    }
  };

  useEffect(() => {
    isLoginHandler();
  });

  return (
    <Navbar
      bg='dark'
      variant='dark'
      expand='lg'
      sticky='top'
      // className="navbar py-0 shadow-lg bg-dark"
      className={styles.containerNav}
    >
      <Container>
        <Navbar.Brand className='w-20 d-flex' href='/'>
          <div>
            <Image
              src='/image/cilokIcon.png'
              className='logo'
              alt='MyCilok'
              width={75}
              height={75}
            />
          </div>
          <div className='mt-4'>MyCilok</div>
        </Navbar.Brand>
        <Navbar.Toggle aria-controls='basic-navbar-nav' />
        <Navbar.Collapse id='basic-navbar-nav'>
          <Nav
            as='ul'
            className='ms-lg-5 justify-content-between'
            style={{ width: '15%' }}
          >
            {/* <Nav.Item as="li"> */}
            <li className={styles.item}>
              <Link href='/'>
                <a className='nav-link'>HOME</a>
              </Link>
              {/* </Nav.Item> */}
            </li>
            <li className={styles.item}>
              <Link href='/game'>
                <a className='nav-link'>GAME</a>
              </Link>
            </li>
          </Nav>
          {isLogin ? (
            <Nav
              as='ul'
              className='ms-lg-auto justify-content-between'
              style={{ width: '20%' }}
            >
              <li className={styles.item}>
                <Link href='/profile'>
                  <a className='nav-link'>
                    PROFILE (
                    {currentUser?.score}
                    )
                  </a>
                </Link>
              </li>
              <li className={styles.item}>
                <a className='nav-link' onClick={() => handleLogout()}>
                  LOGOUT
                </a>
              </li>
            </Nav>
          ) : (
            <Nav
              as='ul'
              className='ms-lg-auto justify-content-between'
              style={{ width: '20%' }}
            >
              <li className={styles.item}>
                <Link href='/register'>
                  <a className='nav-link'>REGISTER</a>
                </Link>
              </li>
              <li className={styles.item}>
                <Link href='/login'>
                  <a className='nav-link'>LOGIN</a>
                </Link>
              </li>
            </Nav>
          )}
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
  // }
};

const mapStateToProps = (state) => ({ currentUser: state.main?.currentUser });

const mapDispatchToProps = {
  setCurrentUser,
};

export default connect(mapStateToProps, mapDispatchToProps)(Navigation);
