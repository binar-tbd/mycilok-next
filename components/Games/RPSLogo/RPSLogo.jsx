/* eslint-disable linebreak-style */
import React from 'react';
import Image from 'next/image';
import styles from './RPSLogo.module.css';

const RPSLogo = () => (
  <Image
    src='/image/rps-logo.png'
    alt='RPS logo'
    className={styles.logo}
    width={48}
    height={48}
  />
);

export default RPSLogo;
