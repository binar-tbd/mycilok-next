/* eslint-disable linebreak-style */
import React from 'react';
import styles from './VersusWrapper.module.css';

const VersusWrapper = () => (
  <div className={`${styles['versus-wrapper']} d-block`} id='versus-wrapper'>
    <h1
      className={`text-uppercase text-center font-weight-bold ${styles['versus-text']}`}
    >
      vs
    </h1>
  </div>
);

export default VersusWrapper;
