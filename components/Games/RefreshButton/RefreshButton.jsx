/* eslint-disable linebreak-style */
import React from 'react';
import { Button } from 'react-bootstrap';
import Image from 'next/image';
import styles from './RefreshButton.module.css';

const RefreshButton = (props) => {
  const { onClick } = props;
  return (
    <Button className={styles['btn-refresh']} onClick={onClick}>
      <Image
        src='/image/refresh.png'
        alt='refresh'
        className={styles.refresh}
        width={96}
        height={96}
      />
    </Button>
  );
};

export default RefreshButton;
