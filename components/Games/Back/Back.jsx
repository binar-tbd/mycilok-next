/* eslint-disable linebreak-style */
import React from 'react';
import Image from 'next/image';
import styles from './Back.module.css';

const Back = (props) => {
  const { onClick } = props;

  return (
    <Image
      loading='eager'
      onClick={onClick}
      src='/image/back.png'
      alt='back'
      className={styles['back-icon']}
      width={48}
      height={48}
    />
  );
};

export default Back;
