/* eslint-disable linebreak-style */
import React, { Fragment } from 'react';
import styles from './Result.module.css';

const Result = (props) => {
  const { result, label } = props;
  return (
    <div className={styles['result-body']} id={`${result}-result`}>
      {result === 'draw' && (
        <h2 className={`text-uppercase p-0 m-0 ${styles['draw-text']}`}>
          {label}
        </h2>
      )}
      {(result === 'player-1' || result === 'com') && (
        <>
          <h2 className={`text-uppercase p-0 m-0 ${styles['player-text']}`}>
            {label}
          </h2>
          <h2 className={`text-uppercase p-0 m-0 ${styles['player-text']}`}>
            win
          </h2>
        </>
      )}
    </div>
  );
};

export default Result;
