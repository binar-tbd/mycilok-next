/* eslint-disable linebreak-style */
/* eslint-disable no-unneeded-ternary */
import React from 'react';

const Title = (props) => {
  const { customClass, label } = props;

  return (
    <h1
      className={`${
        customClass ? customClass : null
      } text-uppercase font-weight-bold`}
    >
      {label}
    </h1>
  );
};

export default Title;
