/* eslint-disable linebreak-style */
/* eslint-disable no-unused-vars */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable linebreak-style */
import React from 'react';
import Image from 'next/image';
import styles from './ChoiceCard.module.css';

const ChoiceCard = (props) => {
  const {
    image, name, type, onClick,
  } = props;

  return (
    <div className={type} id={name} onClick={onClick}>
      <Image
        loading='eager'
        src={image}
        alt={name}
        className={name}
        id={name}
        width={140}
        height={140}
      />
    </div>
  );
};

export default ChoiceCard;
