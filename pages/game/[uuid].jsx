/* eslint-disable new-cap */
/* eslint-disable no-unused-vars */
/* eslint-disable no-param-reassign */
/* eslint-disable react/no-array-index-key */
/* eslint-disable no-return-assign */
/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable no-nested-ternary */
import Link from 'next/link';
import Image from 'next/image';
import { connect } from 'react-redux';
import { useRouter } from 'next/router';
import jsPDF from 'jspdf';
import Navigation from '../../components/Navigation/Navigation';
// import { Badge, Button } from 'react-bootstrap';
// import { useState, useEffect } from 'react';

const GameDetail = (props) => {
  const { currentUser, game } = props;
  const router = useRouter();
  if (router.isFallback) {
    return <div>Loading...</div>;
  }

  const gameLink = game.name === 'RPS' ? `/game/play/${game.uuid}` : '/';
  const gameClass = gameLink === '/' || currentUser === null
    ? 'btn btn-warning shadow-lg disabled'
    : 'btn btn-warning shadow-lg';
  const userGames = currentUser?.user_games || [];
  const isPlayed = userGames.includes(game.uuid);

  const generatePDF = () => {
    const doc = new jsPDF('p', 'pt', 'a3');
    doc.html(document.querySelector('#content'), {
      callback(pdf) {
        pdf.deletePage(9);
        pdf.deletePage(8);
        pdf.deletePage(7);
        pdf.deletePage(6);
        pdf.deletePage(5);
        pdf.deletePage(4);
        pdf.deletePage(3);
        pdf.deletePage(2);
        pdf.save('leaderboard.pdf');
      },
    });
  };

  return (
    <div>
      <Navigation />
      <div className='shadow-lg container-fluid bg-secondary rounded-3 mt-5'>
        <div className='d-flex align-items-baseline'>
          <div className='w-25 text-center text-white my-3 mx-2'>
            {currentUser && isPlayed ? (
              <span className='btn btn-info mb-2'>Already played!</span>
            ) : currentUser && !isPlayed ? (
              <span className='btn btn-light mb-2'>Not played yet!</span>
            ) : (
              <span />
            )}
            <Image
              src={game.image ? game.image : '/image/game3.jpg'}
              width={380}
              height={250}
              alt='game'
            />
            <h1>{game.name}</h1>
            <p>{game.detail}</p>
            <Link href={gameLink}>
              <a className={gameClass}>Play Now!</a>
            </Link>
          </div>
          <div className='w-75 justify-content-center text-center my-3 mx-2' id='content'>
            <h1>Leaderboard</h1>
            <div className='shadow-sm rounded-3 bg-white h-100'>
              {game.name}
              {' '}
              Leaderboard
              <table className='table table-bordered table-hover'>
                <thead>
                  <tr>
                    <th scope='col'>Rank</th>
                    <th scope='col'>Player</th>
                    <th scope='col'>Score</th>
                    <th scope='col'>Action</th>
                  </tr>
                </thead>
                {game.name === 'RPS' && (
                <tbody>
                  {game.leaderboards?.map((item, index) => (
                    <tr key={index}>
                      <th scope='row'>{(index += 1)}</th>
                      <td>{item?.user_profile?.full_name}</td>
                      <td>{item.score}</td>
                      <td>
                        <Link
                          href={`/profile/${item?.user_profile?.user_id}`}
                        >
                          <a className='btn btn-info'>Peek</a>
                        </Link>
                      </td>
                    </tr>
                  ))}
                </tbody>
                )}
                {game.name !== 'RPS' && (
                <tbody>
                  <tr>
                    <th scope='row'>1</th>
                    <td>Mark</td>
                    <td>55</td>
                    <td>
                      <Link href='#'>
                        <a className='btn btn-info disabled'>Peek</a>
                      </Link>
                    </td>
                  </tr>
                  <tr>
                    <th scope='row'>2</th>
                    <td>Jacob</td>
                    <td>43</td>
                    <td>
                      <Link href='#'>
                        <a className='btn btn-info disabled'>Peek</a>
                      </Link>
                    </td>
                  </tr>
                  <tr>
                    <th scope='row'>3</th>
                    <td>Larry the Bird</td>
                    <td>32</td>
                    <td>
                      <Link href='#'>
                        <a className='btn btn-info disabled'>Peek</a>
                      </Link>
                    </td>
                  </tr>
                </tbody>
                )}
              </table>
            </div>
            <button
              onClick={generatePDF}
              type='button'
              className='btn btn-light'
            >
              Generate PDF
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export async function getServerSideProps(context) {
  const { uuid } = context.params;
  const res = await fetch(
    `https://mycilok-backend.herokuapp.com/api/v1/games/${uuid}`,
  );
  // const res = await fetch(`http://localhost:5000/api/v1/games/${uuid}`);
  const game = await res.json();

  return {
    props: {
      game: game.data,
    },
  };
}

const mapStateToProps = (state) => ({ currentUser: state.main?.currentUser });

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(GameDetail);
