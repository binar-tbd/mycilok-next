/* eslint-disable no-unused-vars */
/* eslint-disable no-use-before-define */
/* eslint-disable no-multi-assign */
/* eslint-disable prefer-destructuring */
/* eslint-disable no-shadow */
import {
  Button, Col, Container, Row,
} from 'react-bootstrap';
import React, { useState } from 'react';
import { connect } from 'react-redux';
import { useRouter } from 'next/router';
import {
  Title,
  // Navigation,
  ChoiceCard,
  VersusWrapper,
  RefreshButton,
  Result,
  RPSLogo,
  Back,
} from '../../../components';
import styles from './RPSGame.module.css';
import { setCurrentUser } from '../../../redux/actions/main';
import {
  getComHand,
  getResult,
  setActiveHand,
  setInactiveHand,
} from '../../../helper/helper';

const initialState = {
  versus: true,
  draw: false,
  player1: false,
  com: false,
  playerHand: null,
  comHand: null,
  playerScore: 0,
  comScore: 0,
  winner: null,
  // userProfileId: null,
  // user_id: null,
};

const RPSGame = (props) => {
  const [state, setState] = useState(initialState);
  const { currentUser, setCurrentUser } = props;
  const router = useRouter();
  const { uuid } = router.query;

  const handleClick = (event) => {
    // event.preventDefault();
    let playerScore = state.playerScore;
    let comScore = state.comScore;

    if (!state.playerHand) {
      const playerHand = event.target.id;
      setActiveHand('player', playerHand);

      const comHand = getComHand();
      const result = getResult(playerHand, comHand);

      if (result === 'player1') {
        playerScore = playerScore += 1;
      }

      if (result === 'com') {
        comScore = comScore += 1;
      }

      let winner = null;
      if (playerScore === 3 || comScore === 3) {
        if (playerScore === 3) winner = 'player';
        if (comScore === 3) winner = 'com';
      }

      setState({
        ...state,
        versus: false,
        [result]: true,
        playerHand,
        comHand,
        playerScore,
        comScore,
        winner,
      });

      if (winner) {
        postMatch(winner, playerScore, comScore);
      }
    }
  };

  const postMatch = (winner, playerScore, comScore) => {
    /**
     * Data Dummy
     * ===========
     * player_id harusnya dari userProfileId-nya currentUser
     * game_id harusnya dari url (uuid game)
     */
    // const player_id = "617a8eb6-b078-4cb4-9eba-461a071e0515";
    // const game_id = "617a8eb6-b078-4cb4-9eba-461a071e051a";

    const requestData = {
      player_id: currentUser?.uuid,
      game_id: uuid,
      score: parseFloat(playerScore),
    };

    fetch('https://mycilok-backend.herokuapp.com/api/v1//matches', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        // 'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: JSON.stringify(requestData),
    })
      .then((response) => response.json())
      .then((result) => {
        // console.log(`result`, result);
        if (result.status) {
          setState((prevState) => ({
            ...prevState,
            versus: false,
            // [result]: true,
            playerScore,
            comScore,
            winner,
          }));

          updateCurrentUser();
        }
      })
      .catch((err) => {
        console.log(err.message);
        alert(err.message);
      });
  };

  const updateCurrentUser = () => {
    fetch(
      `https://mycilok-backend.herokuapp.com/api/v1/user-profile/${currentUser.user_id}`,
    )
      .then((response) => response.json())
      .then((result) => {
        const userProfile = result.data;
        setCurrentUser(userProfile);
      })
      .catch((err) => {
        alert(err.message);
      });
  };

  const handleRefresh = () => {
    // event.preventDefault();
    const { playerHand, comHand } = state;

    if (playerHand && comHand) {
      setInactiveHand('com', comHand);
      setInactiveHand('player', playerHand);
      setState({
        ...state,
        versus: true,
        draw: false,
        player1: false,
        com: false,
        playerHand: null,
        comHand: null,
      });
    }
  };

  const handlePlay = (event) => {
    const { playerHand, comHand } = state;
    setInactiveHand('com', comHand);
    setInactiveHand('player', playerHand);
    setState(initialState);
  };

  return (
    <div className={styles['rps-body']}>
      <Container fluid>
        <header className='row pt-3'>
          <div className='col-auto d-flex align-items-center'>
            <Back onClick={() => router.back()} />
          </div>
          <div className='col-auto d-flex align-items-center'>
            <RPSLogo />
          </div>
          <div className='col-9'>
            <Title label='rock paper scissors' customClass={styles.title} />
          </div>
        </header>
      </Container>

      <main className={styles['main-games']}>
        <Row>
          <Col className='d-flex justify-content-center align-items-center text-light'>
            <Title label='player 1' customClass={styles['player-title']} />
          </Col>
          <Col className='d-flex flex-column justify-content-center align-items-center'>
            <h2 className='m-0 text-light'>
              {state.playerScore}
              -
              {' '}
              {state.comScore}
            </h2>
            {state.winner === 'player' && (
              <p className='m-0 bg-success py-1 px-2 text-light rounded'>
                You Win
              </p>
            )}
            {state.winner === 'com' && (
              <p className='m-0 bg-danger py-1 px-2 text-light rounded'>
                You Lose
              </p>
            )}
          </Col>
          <Col className='d-flex justify-content-center align-items-center text-light'>
            <Title label='com' customClass={styles['player-title']} />
          </Col>
        </Row>

        <Row>
          <Col className='d-flex justify-content-center'>
            <ChoiceCard
              name='rock'
              type='player-choice-box'
              image='/image/rock.png'
              onClick={(event) => handleClick(event)}
            />
          </Col>
          <Col />
          <Col className='d-flex justify-content-center'>
            <ChoiceCard
              name='rock'
              type='com-choice-box'
              image='/image/rock.png'
            />
          </Col>
        </Row>

        <Row>
          <Col className='d-flex justify-content-center'>
            <ChoiceCard
              name='paper'
              type='player-choice-box'
              image='/image/paper.png'
              onClick={(event) => handleClick(event)}
            />
          </Col>
          <Col className='d-flex align-items-center justify-content-center text-center'>
            {state.versus && <VersusWrapper />}
            {state.draw && <Result result='draw' label='draw' />}
            {state.player1 && <Result result='player-1' label='you' />}
            {state.com && <Result result='com' label='com' />}
          </Col>
          <Col className='d-flex justify-content-center'>
            <ChoiceCard
              name='paper'
              type='com-choice-box'
              image='/image/paper.png'
            />
          </Col>
        </Row>

        <Row>
          <Col className='d-flex justify-content-center'>
            <ChoiceCard
              name='scissors'
              type='player-choice-box'
              image='/image/scissors.png'
              onClick={(event) => handleClick(event)}
            />
          </Col>
          <Col />
          <Col className='d-flex justify-content-center'>
            <ChoiceCard
              name='scissors'
              type='com-choice-box'
              image='/image/scissors.png'
            />
          </Col>
        </Row>

        <Row>
          <Col />
          <Col className='d-flex justify-content-center'>
            {!state.winner && (
              <RefreshButton onClick={(event) => handleRefresh(event)} />
            )}
            {state.winner && (
              <Button
                variant='warning text-light'
                className='my-5'
                size='lg'
                onClick={(event) => handlePlay(event)}
              >
                Play Again
              </Button>
            )}
          </Col>
          <Col />
        </Row>
      </main>
    </div>
  );
};

const mapStateToProps = (state) => ({ currentUser: state.main?.currentUser });

const mapDispatchToProps = {
  setCurrentUser,
};

export default connect(mapStateToProps, mapDispatchToProps)(RPSGame);
