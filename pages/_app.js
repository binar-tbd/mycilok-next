import '../styles/globals.css';
import 'bootstrap/dist/css/bootstrap.css';
import { wrapper } from '../redux/store';

const MyApp = ({ Component, pageProps }) => (
  // eslint-disable-next-line react/jsx-props-no-spreading
  <Component {...pageProps} />
);

export default wrapper.withRedux(MyApp);
