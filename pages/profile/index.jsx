/* eslint-disable no-shadow */
import { useState, Fragment, useEffect } from 'react';
import { format } from 'date-fns';
import { connect } from 'react-redux';
// import Image from 'next/image';
import { setCurrentUser } from '../../redux/actions/main';
import styles from './profile.module.css';
import Navigation from '../../components/Navigation/Navigation';

const Profile = (props) => {
  const { currentUser, setCurrentUser } = props;
  const [name, setName] = useState('');
  const [fullName, setFullName] = useState('');
  const [email, setEmail] = useState('');
  const [date, setDate] = useState('');
  // const [showResult, setShowResult] = useState(false);
  const [isUpdate, setIsUpdate] = useState(false);
  const [imageChange, setImageChange] = useState(false);
  const [media, setMedia] = useState('');
  const [isLoading, setLoading] = useState(false);

  const getUserProfile = () => {
    fetch(
      `https://mycilok-backend.herokuapp.com/api/v1/user-profile/${currentUser.user_id}`,
      // http://localhost:5000/api/v1/user-profile/${currentUser.user_id},
    )
      .then((response) => response.json())
      .then((result) => {
        console.log('result', result.data);
        const { data } = result;
        if (result.status) {
          setName(data.full_name);
          setFullName(data.full_name);
          setEmail(data.email);
          setDate(() => format(new Date(data.birthday), 'yyyy-MM-dd'));
          setMedia(data.photo_url);
          setCurrentUser(data);
          console.log('media', media);
        }
      })
      .catch((err) => {
        console.log(err.message);
        alert(err.message);
      });
  };

  const imageUpload = async () => {
    const data = new FormData();
    data.append('file', media);
    data.append('upload_preset', 'jlwfwae3');
    data.append('cloud_name', 'timcilok');
    const res = await fetch(
      'https://api.cloudinary.com/v1_1/timcilok/image/upload',
      {
        method: 'POST',
        body: data,
      },
    );
    const res2 = await res.json();
    // alert('upload complete');
    console.log('res2.url', res2.url);
    return res2.url;
  };

  const handleSubmit = async (e) => {
    setLoading(true);
    e.preventDefault();
    try {
      // upload to cloudinary
      const mediaUrl = await imageUpload();
      // console.log(mediaUrl);

      const requestData = {
        full_name: fullName,
        email,
        birthday: date,
        photo_url: mediaUrl,
      };

      console.log('requestData', requestData);
      // const res = await fetch(
      fetch(
        `https://mycilok-backend.herokuapp.com/api/v1/user-profile/${currentUser.user_id}`,
        // http://localhost:5000/api/v1/user-profile/${currentUser.user_id},
        {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(requestData),
        },
      )
        .then((response) => response.json())
        .then((result) => {
          // console.log(result, result);
          if (result.status) {
            getUserProfile();
            // alert('Update Success');
            setIsUpdate(false);
            setImageChange(false);
            setLoading(false);
          }
          // console.log(result, result);
        })
        .catch((err) => {
          console.log(err.message);
          alert(err.message);
          setLoading(false);
        });
    } catch (err) {
      console.log(err);
      setLoading(false);
    }
  };

  const handleEdit = () => {
    setIsUpdate(!isUpdate);
  };

  const handleChange = (e) => {
    setMedia(e.target.files[0]);
    setImageChange(true);
  };

  useEffect(() => {
    getUserProfile();
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  return (
    <>
      <Navigation />
      <div className={styles.profilebody}>
        <div className={styles.wrapper}>
          <div className={styles.left}>
            <div className='file-field input-field'>
              <div className='btn #1565c0'>
                {/* eslint-disable-next-line jsx-a11y/label-has-associated-control */}
                <label htmlFor='image_uploads' hidden={!isUpdate}>
                  Change photo
                </label>
                <input
                  id='image_uploads'
                  type='file'
                  accept='image/*'
                  onChange={handleChange}
                  hidden
                />
              </div>

              {/* <Image
                alt='user'
                width={180}
                height={200}
                // src={media ? URL.createObjectURL(media) : '/image/profile.jpeg'}
                // src={imageUrl ? imageUrl : '/image/profile.jpeg'}
                src={
                  media
                    ? imageChange
                      ? URL.createObjectURL(media)
                      : media
                    : '/image/profile.jpeg'
                }
              /> */}

              {/* eslint-disable-next-line @next/next/no-img-element */}
              <img
                alt='user'
                className='responsive-img'
                width={180}
                height={200}
                // src={media ? URL.createObjectURL(media) : '/image/profile.jpeg'}
                // src={media ? media : '/image/profile.jpeg'}
                src={
                  // eslint-disable-next-line no-nested-ternary
                  media
                    ? imageChange
                      ? URL.createObjectURL(media)
                      : media
                    : '/image/profile.jpeg'
                }
              />

              <h4>{name || 'username'}</h4>
            </div>
          </div>
          <form onSubmit={(e) => handleSubmit(e)}>
            <div className={styles.right}>
              <div className={styles.right}>
                <div className={styles.info}>
                  <h3>Information</h3>
                  <div className={styles.info_data}>
                    <div className={styles.info_data}>
                      <div className={styles.data}>
                        <h4>Username</h4>
                        <input
                          id='username'
                          type='text'
                          name='fullname'
                          className=''
                          placeholder='username'
                          defaultValue={fullName}
                          //   onChange={changeHandler()}
                          onChange={(e) => {
                            setFullName(e.target.value);
                          }}
                          disabled={!isUpdate}
                        />
                      </div>
                      <div className='data'>
                        <h4>Email</h4>
                        <input
                          id='email'
                          type='email'
                          name='email'
                          className=''
                          placeholder='email'
                          defaultValue={email}
                          //   onChange={changeHandler()}
                          onChange={(e) => {
                            setEmail(e.target.value);
                          }}
                          disabled
                        />
                      </div>
                      <div className='data'>
                        <h4>Date of Birth</h4>
                        <input
                          id='date'
                          type='date'
                          name='date'
                          className=''
                          placeholder=''
                          defaultValue={date}
                          //   onChange={changeHandler()}
                          onChange={(e) => {
                            setDate(e.target.value);
                          }}
                          disabled={!isUpdate}
                        />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className={styles.mainbutton}>
                <div className={styles.onebutton}>
                  {!isLoading && (
                    <button
                      type='submit'
                      disabled={!isUpdate}
                      className={styles.profilebutton}
                    >
                      Update
                    </button>
                  )}
                  {isLoading && (
                    <button
                      type='submit'
                      className={styles.profilebutton}
                      disabled
                    >
                      <span
                        className='spinner-border'
                        role='status'
                        aria-hidden='true'
                      />
                      {' '}
                      Processing...
                    </button>
                  )}
                </div>

                <div className={styles.secondbutton}>
                  <button
                    type='submit'
                    disabled={isUpdate}
                    // onClick={handleEdit()}
                    onClick={(e) => handleEdit(e)}
                    className={styles.profilebutton}
                  >
                    Edit
                  </button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </>
  );
};
const mapStateToProps = (state) => ({ currentUser: state.main?.currentUser });

const mapDispatchToProps = {
  setCurrentUser,
};

export default connect(mapStateToProps, mapDispatchToProps)(Profile);
