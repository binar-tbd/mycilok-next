/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
/* eslint-disable jsx-a11y/label-has-associated-control */
import React, { Fragment, useState, useEffect } from 'react';
// import { profileImage } from "../../assets/image";
// import { Link } from "react-router-dom";
// import { AuthContext } from "../../auth/auth";
// import Navigation from "../../components/Navigation";
import { format } from 'date-fns';
// import { bgImage } from "../../assets/image";
import { useRouter } from 'next/router';
import { connect } from 'react-redux';
import Image from 'next/image';
import { Button } from 'react-bootstrap';
import Navigation from '../../components/Navigation/Navigation';
import styles from './profile.module.css';

const ProfileUuid = (props) => {
  const { currentUser } = props;
  const [state, setState] = useState({
    name: '',
    fullname: '',
    email: '',
    date: '',
    media: '',
    showResult: false,
    isUpdate: false,
  });
  const router = useRouter();
  const { uuid } = router.query;

  const getUserProfile = () => {
    fetch(`https://mycilok-backend.herokuapp.com/api/v1/user-profile/${uuid}`)
      .then((response) => response.json())
      .then((result) => {
        const { data } = result;
        if (result.status) {
          setState({
            ...state,
            name: data.full_name,
            fullname: data.full_name,
            date: format(new Date(data.birthday), 'yyyy-MM-dd'),
            email: data.email,
            media: data.photo_url,
          });
        }
      })
      .catch((err) => {
        console.log(err.message);
        alert(err.message);
      });
  };

  useEffect(() => {
    getUserProfile();
  }, []);

  return (
    <>
      <Navigation />
      <div className={styles.profilebody}>
        <div className={styles.wrapper}>
          <div className={styles.left}>
            {/* <img src="" alt="user" width="200" /> */}
            <Image
              src={state.media || '/image/profile.jpeg'}
              alt='user'
              width={200}
              height={200}
            />
            <h4>{state.name || 'username'}</h4>
          </div>
          <form>
            <div className={styles.right}>
              <div className={styles.right}>
                <div className={styles.info}>
                  <h3 className='text-info'>User Info</h3>
                  <div className={styles.info_data}>
                    <div className={styles.info_data}>
                      <div className={styles.data}>
                        <h4 className='text-info'>Username</h4>
                        <label>
                          {' '}
                          {state.fullname}
                        </label>
                      </div>
                      <div className={styles.data}>
                        <h4 className='text-info'>Email</h4>
                        <label>{state.email}</label>
                      </div>
                      <div className={styles.data}>
                        <h4 className='text-info'>Date of Birth</h4>
                        <label>{state.date}</label>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className={styles.mainbutton}>
                {/* <Link href="/game" className="btn-info">
                  Back
                </Link> */}
                <Button variant='info' onClick={() => router.back()}>
                  Back
                </Button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </>
  );
};

const mapStateToProps = (state) => ({ currentUser: state.main?.currentUser });

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(ProfileUuid);
